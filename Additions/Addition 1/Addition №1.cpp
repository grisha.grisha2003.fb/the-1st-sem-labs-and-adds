﻿#include <iostream>

int main()
{
	int FinalCost = 0;
	int Q1, P1, Q2, P2, A;
	std::cin >> Q1 >>  P1 >> Q2 >> P2 >>  A;
	float Coef1 = Q1 / P1;
	float Coef2 = Q2 / P2;
	float MinCoef = std::min(Coef1, Coef2);
	float MinCoefP;
	float MinCoefQ;

	if (Coef1 < 1 and Coef2 < 1)
	{
		if (MinCoef * P1 == Q1)
		{
			MinCoefP = P1;
			MinCoefQ = Q1;
		}
		else
		{
			MinCoefP = P2;
			MinCoefQ = Q2;
		}
		while (A >= MinCoefQ)
		{
			FinalCost = FinalCost + MinCoefP;
			A = A - MinCoefQ;
		}
	}
	else
	{
		if (MinCoef * P1 == Q1)
		{
			MinCoefP = P2;
			MinCoefQ = Q2;
		}
		else
		{
			MinCoefP = P1;
			MinCoefQ = Q1;
		}
		while (A >= MinCoefQ)
		{
			FinalCost = FinalCost + MinCoefP;
			A = A - MinCoefQ;
		}
	}
	
	//На выходе A - остаток груза, FinalCost - сумма, за который отвезли излишки.


	//Теперь без всяких усложнений просто расчитаем 2 варианта и выберем наименьший
	int X1, X2;
	X1 = P1 * (ceil(float(A) / float(Q1)));
	X2 = P2 * (ceil(float(A) / float(Q2)));
	FinalCost += std::min(X1, X2);
	
	std::cout << FinalCost;
	
	return 0;
}