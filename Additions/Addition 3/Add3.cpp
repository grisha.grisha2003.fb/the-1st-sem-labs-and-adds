﻿/*Вариант 6.
Дано натуральное число до 10^9.
Определите минимальную цифру числа,
самую редко встречающуюся цифру (если таких несколько, выведите наименьшую) 
и произведение цифр числа.
*/

#include <iostream>
#include "Functions.hpp"



int main()
{
    int x = Func::Input();
    Func::Output(Func::MinNumber(x), Func::TheRarestNumber(x), Func::MultiplicationOfNumbers(x));
    return 0;
}

