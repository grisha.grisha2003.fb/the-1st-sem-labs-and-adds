#pragma once
namespace Func 
{
	long int Input();

	int MinNumber(long int x);

	int TheRarestNumber(long int x);

	long int MultiplicationOfNumbers(long int  x);

	void Output(int q1, int q2, long int q3);
}