#include <iostream>
#include "Functions.hpp"
namespace Func
{
    long int Input()
    {
        setlocale(LC_ALL, "Rus");
        std::cout << "������� �����: ";
        long int x;
        std::cin >> x;
        return x;
    }

    int MinNumber(long int x)
    {
        int MN = INT_MAX;
        int i;
        while (x > 0)
        {
            i = x % 10;
            if (i < MN)
                MN = i;
            x = x / 10;
        }
        return MN;
    }


    int TheRarestNumber(long int x)
    {
        int mas[10];
        for (int i = 0; i < 10; i++)
            mas[i] = 0;

        int i;
        while (x > 0)
        {
            i = x % 10;
            mas[i] = mas[i] + 1;
            x = x / 10;
        }

        int j = INT_MAX;
        int RarestNumb;
        for (int i = 0; i < 10; i++)
        {
            if (mas[i] < j && mas[i] != 0)
            {
                RarestNumb = i;
                j = mas[i];
            }
        }
        return RarestNumb;
    }

    long int MultiplicationOfNumbers(long int x)
    {
        long int  MON = 1;
        while (x > 0)
        {
            int i = x % 10;
            MON = MON * i;
            x = x / 10;
        }
        return MON;
    }

    void Output(int q1, int q2, long int q3)
    {
        std::cout << "����������� ����� �����:" << q1 << std::endl;
        std::cout << "����� ������ �����:" << q2 << std::endl;
        std::cout << "������������ ���� �����:" << q3 << std::endl;
    }
}