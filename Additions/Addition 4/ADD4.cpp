﻿#include <iostream>

int main()
{
    const unsigned maxSumm = 27;

    unsigned summs[maxSumm + 1] = { 0 };

    for (int i = 0; i < 10; ++i)
    {
        for (int j = 0; j < 10; ++j)
        {
            for (int k = 0; k < 10; ++k)
            {
                ++summs[i + j + k];
            }
        }
    }
    unsigned count = 0;
    for (int i = 1; i <= maxSumm; ++i)
    {
        count += summs[i] * summs[i];
    }

    std::cout <<  count << std::endl;

    return 0;
}
