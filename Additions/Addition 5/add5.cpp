﻿/*Вариант-6. 
Дана последовательность натуральных чисел {Aj}j=1...n (n<=10000).
Удалить из последовательности числа, начинающиеся цифрой 2,
а среди оставшихся продублировать числа, все цифры которых различны.
*/

#include <iostream>
#define N 20000

void InputMas(int mas[N], int n)
{
	for (int i = 0; i < 2 * n; i += 2) // i+=2 : Для каждого числа оставляем следущую позицию пустой на случай дублирования
		std::cin >> mas[i];
}

void OutputMas(int mas[N] ,int n )
{
	for (int i = 0; i < 2 * n; i++)
		if (mas[i] != 0)
			std::cout << mas[i] << ' ';
}

void DelNums(int mas[N], int n)
{
	for (int i = 0; i < 2 * n; i += 2)
	{
		int j = mas[i];
		while (j > 10)
			j = j / 10;

		if (j == 2)
			mas[i] = 0;
	}
}

void Duplicate(int mas[N], int n)
{
	for (int i = 0; i < 2 * n; i += 2)
	{
		short int a = 0, a0 = 0, a1 = 0, a2 = 0 , a3 = 0, a4 = 0, a5 = 0, a6 = 0, a7 = 0, a8 = 0, a9 = 0;

		int j = mas[i];

		while (j > 0) // я бы использовал вспомогательный массив, но по условию нельзя (
		{
			a = j % 10;
			j = j / 10;

			switch (a)
			{
			case 0: a0++; break;
			case 1: a1++; break;
			case 2: a2++; break;
			case 3: a3++; break;
			case 4: a4++; break;
			case 5: a5++; break;
			case 6: a6++; break;
			case 7: a7++; break;
			case 8: a8++; break;
			case 9: a9++; break;
			default:
				break;
			}
		}

		if (a0 != 2 && a1 != 2 && a2 != 2 && a3 != 2 && a4 != 2 && a5 != 2 && a6 != 2 && a7 != 2 && a8 != 2 && a9 != 2)
			mas[i + 1] = mas[i];
	}
}

int main()
{
	setlocale(LC_ALL, "Rus");
	std::cout << "Введите количество символов: ";
	int n;
	std::cin >> n;


	int mas[N];
	for (int i = 0; i < N; i++)
		mas[i] = 0;

	InputMas(mas, n);

	DelNums(mas, n);

	Duplicate(mas, n);

	OutputMas(mas, n);

	return 0;
}