﻿/*Вариант-6.
Дана последовательность натуральных чисел {Aj}j=1...n (n<=10000).
Удалить из последовательности числа, начинающиеся цифрой 2,
а среди оставшихся продублировать числа, все цифры которых различны.
*/

#include <iostream>
#include <vector>


bool DelNums(int vec_i)
{
	int j = vec_i;
	while (j > 10)
		j = j / 10;

	if (j == 2)
		return true;
	else
		return false;
}

bool Duplicate(int vec_i)
{
	int mas[10];
	for (int j = 0; j < 10; j++)
		mas[j] = 0;

	int x = vec_i;
	int a;
	while (x > 0)
	{
		a = x % 10;
		x = x / 10;

		mas[a]++;
	}

	bool difnums = true;
	for (int j = 0; j < 10; j++)
		if (mas[j] > 1)
			difnums = false;
		
	return difnums;
}



int main()
{
	setlocale(LC_ALL, "ru");
	
	int n;
	std::cout << "Введите количество элементов: ";
	std::cin >> n;

	std::vector<int> vec(0);

	for (int i = 0; i < n; i++)
	{
		int x;
		std::cin >> x;
		vec.push_back(x);
	}

	for (int i = 0; i < vec.size(); i++)
		if (DelNums(vec[i]))
		{
			vec.erase(vec.begin() + i);
			i--;
		}

	for (int i = 0; i < vec.size(); i++)
		if (Duplicate(vec[i]))
		{
			vec.insert(vec.begin() + i + 1, vec[i]);
			i++;
		}

	for (int i = 0; i < vec.size(); i++)
	{
		std::cout << vec[i] << ' ';
	}

	return 0;
}
