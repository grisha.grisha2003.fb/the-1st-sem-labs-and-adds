﻿#include <iostream>
#include "Rhombus.hpp"

int main()
{
	setlocale(LC_ALL, "Rus");
	float H, A;

	std::cout << "Введите высоту ромба H ( H > 0)" << std::endl;
	std::cin >> H;
	std::cout << "Введите сторону ромба A ( A > 0)" << std::endl;
	std::cin >> A;

	pg::Rhombus R(H, A);

	std::cout << "Площадь ромба: " << R.S() << " Периметр: " << R.P() << std::endl;
	return 0;
}
