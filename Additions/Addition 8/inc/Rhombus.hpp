#pragma once
#include <iostream>

namespace pg
{
	class Rhombus
	{
	public:

		Rhombus(float H, float A);

		~Rhombus();

		float S();
		float P();

	private:
		float m_H;
		float m_A;
	};
}