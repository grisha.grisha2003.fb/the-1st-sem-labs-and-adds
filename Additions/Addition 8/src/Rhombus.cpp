#include <iostream>
#include "Rhombus.hpp"

namespace pg
{
		Rhombus::Rhombus(float H, float A)
		{
			if (H <= 0)
			{
				std::cout << "������ ����� �� ����� ���� ������ 0, ������ �������� �� ���������: 1" << std::endl;
				m_H = 1;
			}
			else
				m_H = H;
			if (A <= 0)
			{
				std::cout << "������� ����� �� ����� ���� ������ 0, ������ �������� �� ���������: 1" << std::endl;
				m_A = 1;
			}
			else
				m_A = A;
		}

		Rhombus::~Rhombus() {}

		float Rhombus::S() { return m_H * m_A; }
		float Rhombus::P() { return m_A * 4; }

}