#pragma once


namespace pg
{
    class Player
    {
    public:
        Player(int x, int y, sf::Texture playerTexture, int G, int height);

        ~Player();

        sf::Sprite GetPlayerSprite();

        void SetPosition(int x, int y);
        void SetPositionX(int x);
        void SetPositionY(int y);

        void SetG(int G);

        int LB();
        int RB();
        int BB();

        sf::Texture GetTexture();
        int GetG();
        int GetPositionX();
        int GetPositionY();
        int GetHeight();

    private:
        int PLAYER_LEFT_BOUNDING_BOX = 5;
        int PLAYER_RIGHT_BOUNDING_BOX;
        int PLAYER_BOTTOM_BOUNDING_BOX;
        int m_PositionX;
        int m_PositionY;
        int m_G;
        int m_height;
        sf::Texture m_PlayerTexture;
        sf::Sprite m_Player_Sprite;

    };




    class Platform
    {
    public:
        Platform(int x, int y, sf::Texture platformTexture);

        ~Platform();

        sf::Sprite GetPlatformSprite();

        void SetPositionY(int y);
        void SetPositionX(int X);

        int GetPositionX();
        int GetPositionY();
        sf::Texture GetTexture();

    private:
        int m_PositionX;
        int m_PositionY;
        sf::Sprite m_Platform_Sprite;
        sf::Texture m_PlatformTexture;

    };
}