#pragma once
#include "Classes.hpp"

namespace pg 
{

    int Input();

    bool IsOnPlatform(Platform platform, Player player);
}