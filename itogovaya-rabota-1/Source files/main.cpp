﻿#include <iostream>
#include <random>
#include <ctime>
#include <SFML/Graphics.hpp>
#include <vector>
#include "Functions.hpp"
#include "Classes.hpp"


int main()
{
    std::random_device rd;
    std::mt19937 gen(rd());
    //Рендер Окна
    sf::RenderWindow window(sf::VideoMode(480, 600), "First Game!");
    window.setFramerateLimit(60);

    //Инициализация текстур и спрайта бэкграунда.
    sf::Texture backgroundTexture;
    sf::Texture playerTexture;
    sf::Texture platformTexture;
    backgroundTexture.loadFromFile("img/background.png");
    playerTexture.loadFromFile("img/doodle.png");
    platformTexture.loadFromFile("img/platform.png");
    sf::Sprite background_sprite(backgroundTexture);
    background_sprite.setPosition(0, 0);

    //Инициализация игрока
    pg::Player player((window.getSize().x - playerTexture.getSize().x) / 2, (window.getSize().y - playerTexture.getSize().y) / 3, playerTexture, -20, 125);


    //Инициализация платформ
    std::vector<pg::Platform> Platforms;
    for (int i = 0; i < 7; i++)
    {
        std::uniform_int_distribution<int> x(0, window.getSize().x - platformTexture.getSize().x);
        std::uniform_int_distribution<int> y(100, window.getSize().y);
        Platforms.push_back(pg::Platform(x(gen),y(gen), platformTexture));
    }

    bool Game = true;
    while (window.isOpen() && Game == true)
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        player.SetPositionX(player.GetPositionX() + pg::Input());
        if (player.GetPositionX() > 480 )
            player.SetPositionX(-76);
        if (player.GetPositionX() < -76)
            player.SetPositionX(480);

        
        player.SetG(player.GetG() + 1);
        player.SetPositionY(player.GetPositionY() + player.GetG());
        if (player.GetPositionY() < player.GetHeight())
            for (int i = 0; i < Platforms.size(); i++)
            {
                //Перемещение платформ вниз
                player.SetPositionY(player.GetHeight());
                Platforms[i].SetPositionY(Platforms[i].GetPositionY() - player.GetG());
               
                //Создание новых платформ
                if (Platforms[i].GetPositionY() > window.getSize().y)
                {
                    Platforms[i].SetPositionY(0);
                    std::uniform_int_distribution<int> x(0, window.getSize().x - platformTexture.getSize().x);
                    Platforms[i].SetPositionX(x(gen));
                }
            }

        //Регистрация попадания на платформу

        for (int i = 0; i < Platforms.size(); i++)
        {
            if (pg::IsOnPlatform(Platforms[i], player))
                player.SetG(-25);
        }

        if (player.GetPositionY() > window.getSize().y + 100)
            Game = false;


        window.draw(background_sprite);
        for(int i = 0; i < Platforms.size(); i++)
            window.draw(Platforms[i].GetPlatformSprite());
        window.draw(player.GetPlayerSprite());
        window.display();






    }
    return 0;
}
