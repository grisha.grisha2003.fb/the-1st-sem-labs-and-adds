#include <iostream>
#include <random>
#include <ctime>
#include <SFML/Graphics.hpp>
#include <vector>
#include "Classes.hpp"
#include "Functions.hpp"



namespace pg
{

    Player::Player(int x, int y, sf::Texture playerTexture, int G, int height)
    {
        m_PositionX = x;
        m_PositionY = y;
        m_G = G;
        m_height = height;
        m_PlayerTexture = playerTexture;
        sf::Sprite player_sprite(m_PlayerTexture);
        player_sprite.setPosition(m_PositionX, m_PositionY);
        m_Player_Sprite = player_sprite;
        std::cout << "Player sprite created!";
        PLAYER_BOTTOM_BOUNDING_BOX = playerTexture.getSize().y;
        PLAYER_RIGHT_BOUNDING_BOX = playerTexture.getSize().x - 5;
    }

    Player::~Player() {}

    sf::Sprite Player::GetPlayerSprite()
    {
        return m_Player_Sprite;
    }

    void Player::SetPosition(int x, int y)
    {
        m_PositionX = x;
        m_PositionY = y;
        m_Player_Sprite.setPosition(m_PositionX, m_PositionY);
    }

    void Player::SetPositionX(int x) { m_PositionX = x; m_Player_Sprite.setPosition(m_PositionX, m_PositionY); }
    void Player::SetPositionY(int y) { m_PositionY = y; m_Player_Sprite.setPosition(m_PositionX, m_PositionY); }
    void Player::SetG(int G) { m_G = G; }



    int Player::LB() { return PLAYER_LEFT_BOUNDING_BOX; }
    int Player::RB() { return PLAYER_RIGHT_BOUNDING_BOX; }
    int Player::BB() { return PLAYER_BOTTOM_BOUNDING_BOX; }
    sf::Texture Player::GetTexture() { return m_PlayerTexture; }
    int Player::GetG() { return m_G; }
    int Player::GetPositionX() { return m_PositionX; }
    int Player::GetPositionY() { return m_PositionY; }
    int Player::GetHeight() { return m_height; };



    Platform::Platform(int x, int y, sf::Texture platformTexture)
    {
        m_PositionX = x;
        m_PositionY = y;
        m_PlatformTexture = platformTexture;
        sf::Sprite platform_sprite(platformTexture);
        platform_sprite.setPosition(m_PositionX, m_PositionY);
        m_Platform_Sprite = platform_sprite;
    }

    Platform::~Platform() {}

    sf::Sprite Platform::GetPlatformSprite()
    {
        return m_Platform_Sprite;
    }

    void Platform::SetPositionY(int y)
    {
        m_PositionY = y;
        m_Platform_Sprite.setPosition(m_PositionX, m_PositionY);
    }
    void Platform::SetPositionX(int X)
    {
        m_PositionX = X;
        m_Platform_Sprite.setPosition(m_PositionX, m_PositionY);
    }
    sf::Texture Platform::GetTexture() { return m_PlatformTexture; };

    int Platform::GetPositionX() { return m_PositionX; };
    int Platform::GetPositionY() { return m_PositionY; };
}
