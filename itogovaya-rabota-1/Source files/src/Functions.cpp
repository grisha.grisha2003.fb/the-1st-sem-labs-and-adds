#include <iostream>
#include <random>
#include <ctime>
#include <SFML/Graphics.hpp>
#include <vector>
#include "Classes.hpp"
#include "Functions.hpp"

namespace pg
{

    int Input()
    {
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::A) || sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
            return -10;

        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D) || sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
            return +10;
        else
            return 0;
    }

    bool IsOnPlatform(Platform platform, Player player)
    {
        if ((player.GetPositionX() + player.RB() > platform.GetPositionX() && (player.GetPositionX() + player.LB() < platform.GetPositionX() + platform.GetTexture().getSize().x) // �������������� ��������, � ������� ����� ����� ��������� ���������
            && (player.GetPositionY() + player.BB() > platform.GetPositionY() && (player.GetPositionY() + player.BB() < platform.GetPositionY() + 2 * platform.GetTexture().getSize().y) // ������������ ��������
                && (player.GetG() > 0))))
            return true;
        else
            return false;
    }
}