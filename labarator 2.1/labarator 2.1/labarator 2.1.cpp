﻿//Вариант 6.
//Дана строка длины N.Если в ней есть хотя бы одна гласная, отсортируйте буквы по алфавиту.


#include <iostream>
#include <string>
#include <Windows.h>
#include <chrono>

class Timer
{
private:
    // Псевдонимы типов используются для удобного доступа к вложенным типам
    using clock_t = std::chrono::high_resolution_clock;
    using second_t = std::chrono::duration<double, std::ratio<1> >;

    std::chrono::time_point<clock_t> m_beg;

public:
    Timer() : m_beg(clock_t::now())
    {
    }

    void reset()
    {
        m_beg = clock_t::now();
    }

    double elapsed() const
    {
        return std::chrono::duration_cast<second_t>(clock_t::now() - m_beg).count();
    }
};


std::string SwapSortStringABS(std::string& string)
{
    std::string alphabet = "аАбБвВгГдДеЕёЁжЖзЗиИйЙкКлЛмМнНоОпПрРсСтТуУфФхХцЦчЧшШщЩъЪыЫьЬэЭюЮяЯ";
    int n;
    n = string.size();

    
    
    for (int i = 0; i < n - 1; i++)
    {
        for (int j = i; j < n; j++)                          // ~ n^2 Op.
        {
            int jsym;
            for (int j2 = 0; j2 < alphabet.size(); j2++)     //
                if (string[j] == alphabet[j2])               //
                {                                            //
                    jsym = j2;                               //
                    break;                                   // ~ 2 * 66 * 4  = 528
                }                                            //
                                                             // 
            int isym;                                        //
            for (int i2 = 0; i2 < alphabet.size(); i2++)     //
                if (string[i] == alphabet[i2])               //
                {
                    isym = i2;
                    break;
                }

            if (isym > jsym)                                 // 4 операции
            {
                int voidChar;
                voidChar = string[i];
                string[i] = string[j];
                string[j] = voidChar;
            }
        }
    }
    return string;                                           // (528 + 4) * n^2
}



int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    setlocale(LC_ALL, "Rus");
    std::string glas = "аАуУоОиИэЭыЫ";
    std::string string;
    std::cout << "Введите строку: ";
    std::cin >> string;
    int n;
    n = string.size();

    bool IsGlas = false;

    Timer timer; //Начало отсчёта

    for (int i = 0; i < n; i++)
        for (int j = 0; j < glas.size(); j++)
        {
       
            if (string[i] == glas[j])         // ~ 12*3*n операций => 36*n
                IsGlas = true;

        }
  
    if (IsGlas)
    {
        string = SwapSortStringABS(string);

        std::cout << timer.elapsed() << std::endl; //Конец отсчёта
             
        std::cout << std::endl << string;
    }
    else
        std::cout << std::endl << "В строке нет гласных";
  

    return 0;                                               // итого: 528 n^2 + 36 n операций - Худший и средний случаи.  n^2 + n - Лучший случай.
                                                            // При условии n = 1000 расчётное время - 5.28 секунд, фактическое - 13.7929 (без break в циклах) и 3.97081 (с break в циклах)
                                                            //  ((в очередной раз убеждаюсь в пользе Break))
}

