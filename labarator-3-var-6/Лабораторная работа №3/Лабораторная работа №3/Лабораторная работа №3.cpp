﻿// Вариант-6. Дана последовательность целых чисел {Aj}.
//Найти сумму отрицательных чисел, кратных 7, наибольшее из таких чисел и номер этого числа в последовательности.


#include <iostream>

int main()
{
	setlocale (LC_ALL, "Russian");
	int A; int SummOtr = 0; int Maxim = INT_MIN; int NumCh = 0;
	
	int n;
	std::cin >> n;
	for (int i = 0; i < n; i++)
	{
		std::cin >> A;
		if ((A < 0) && (A % 7 == 0))
		{
			SummOtr = SummOtr + A;
			
			if (Maxim < A)
			{
				Maxim = A;
				NumCh = i;
			}	
		}
	}
	std::cout << SummOtr <<" " << Maxim <<" "<< NumCh+1;
	return 0;
}

